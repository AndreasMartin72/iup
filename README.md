IUP
===

This is just a copy of the original project located here:
[sourceforge][https://sourceforge.net/projects/iup]

**Short description:**
IUP is a portable toolkit for building graphical user interfaces. It offers a configuration API in three basic languages: C, Lua and LED. IUP purpose is to allow a program to be executed in different systems without any modification.